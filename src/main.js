import Vue from 'vue';
import App from './App.vue';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

// Helpers
import HShortBy from './helpers/ShortBy';

Vue.config.productionTip = false;

HShortBy.activate();

setTimeout(() => {
  new Vue({
    render: h => h(App),
  }).$mount('#app')
}, 3000 );
