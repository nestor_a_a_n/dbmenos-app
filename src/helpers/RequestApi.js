// Externals libs
import axios from 'axios';

// Helpers

// Constants
import API from '../constants/Api';

export default {
  send : async ( config ) => {

    const { endPoint, method = "get", data={}, headers = {}, callbackSuccess = () => {}, callbackError = () => {} } =  config;    

    await axios({
      method,
      url:  `${API.BASE_API}/${endPoint}`,
      data,
      headers
    }).then( response => {
      callbackSuccess( response );
    }).catch( error => {
      let errorText = null;
      if (error.response) {
        if ( error.response.data ) {
          // Request made and server responded          
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          errorText = error.response.data.error.messages;
        }
      } else if (error.request) {
        // The request was made but no response was received
        console.log(error.request);
        errorText = error.request;
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
        errorText = error.message;
      }
      callbackError( errorText );
    });
  }
}