export default {
  activate() {
    (function () {
      if (typeof Object.defineProperty === 'function') {
        Object.defineProperty(Array.prototype, 'sortBy', { value: sortBy });
      }
      if (!Array.prototype.sortBy) {
        Array.prototype.sortBy = sortBy;
      }

      var _DESC = (/^desc:\s*/i);

      // comparison criteria
      function _comparer (prev, next) {
        var asc = 1
        if (typeof prev === 'string') {
          if (_DESC.test(prev)) asc = -1;
        }
        if (prev === next) return 0;
        return (prev > next ? 1 : -1) * asc;
      }

      function sortBy(parser) {
        var i;
        var item;
        var arrLength = this.length;
        if (typeof parser !== 'function') {
          return this.sort(Array.prototype.sort.bind(this));
        }
        // Schwartzian transform (decorate-sort-undecorate)
        for (i = arrLength; i;) {
          item = this[i -= 1];
          // decorate the array
          this[i] = [].concat(parser.call(null, item, i), item);
        }
                // sort the array
        this.sort(function (prev, next) {
          var sorted;
          var length = prev.length;
          for (i = 0; i < length; i += 1) {
            sorted = _comparer(prev[i], next[i])
            if (sorted) return sorted;
          }
          return 0;
        })
                // undecorate the array
        for (i = arrLength; i;) {
          item = this[i -= 1];
          this[i] = item[item.length - 1];
        }
        return this;
      }
    }());
  }
}