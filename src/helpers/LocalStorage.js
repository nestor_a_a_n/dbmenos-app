/**
 * Permitira gestionar todos los valores globales de la aplicacion y que seran almacenados en local storage
 */

// constants

export default (function () {
  var data = null;
  var timerSaveData = null;
  const nameStorage = 'dbmenos';

  /**
  * Called when the signed in status changes, to update the UI
  * appropriately. After a sign-in, the API is called.
  */
  var getData = function() {
    if (data === null) {
      data = localStorage.getItem( nameStorage );
      if ( data ) {
        data = JSON.parse( data );
      } else {
        data = {};
      }
    }
  }

  var saveChanges = function( timer, callback ) {
    if (typeof timer === 'undefined') {
      timer = 200;
    }
    if (timerSaveData) {
      clearTimeout( timerSaveData );
    }
    timerSaveData = setTimeout(() => {
      localStorage.setItem( nameStorage, JSON.stringify( data ));
      if (callback) {
        callback( data );
      }
    }, timer);
  }
  return {
    /**
     * Obtener el valor de la propiedad
     * @param {String} prop propiedad
     * @returns {Any} valor de la propiedad
     */
    get( prop, defaultValue ) {
      getData();
      if ( !data[prop] ) {
        data[prop] = typeof defaultValue === "undefined" ? null : defaultValue;
      }
      return data[prop];
    },
    set( prop, value, timer ) {
      getData();
      data[prop] = value;      
      saveChanges( timer );
    },
    /**
     * 
     */
    setProps( props, timer, callback ) {
      getData();
      for (const key in props) {
        data[key] = props[key];
      }
      saveChanges( timer, callback );
    }
  }
})();