const CHECKED = {
  YES: "1",
  NO: "0"
}

const ORDER = {
  ASC: "1",
  DESC: "0"
}

const CATEGORIES = {
  FILMS: "films",
  PEOPLE: "people",
  LOCATIONS: "locations",
  SPECIES: "species",
  VEHICLES: "vehicles"
}

const DEFAULT = {
  word: "",
  limit: 50,
  order: ORDER.ASC,
  category: CATEGORIES.FILMS
}

export default {
  CHECKED,
  ORDER,
  DEFAULT,
  CATEGORIES
}